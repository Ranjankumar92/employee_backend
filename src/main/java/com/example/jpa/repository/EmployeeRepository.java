package com.example.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.jpa.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{

	void findAllByOrderByFirstName();

}
